//! GeoIP service to get location from an IP address.
//!


#[macro_use]
extern crate serde_derive;

use std::borrow::Borrow;
use std::env;
use std::net::Ipv4Addr;
use std::net::Ipv6Addr;
use std::sync::Arc;

use actix_cors::Cors;
use actix_web::http::HeaderMap;
use actix_web::web;
use actix_web::App;
use actix_web::HttpRequest;
use actix_web::HttpResponse;
use actix_web::HttpServer;
use maxminddb::geoip2::City;
use maxminddb::MaxMindDBError;
use maxminddb::Reader;
// use memmap::Mmap;
use serde_json;

#[derive(Serialize)]
struct NonResolvedIPResponse<'a> {
    pub ip_address: &'a str,
}

#[derive(Serialize)]
struct ResolvedIPResponse<'a> {
    pub ip_address: &'a str,
    pub latitude: &'a f64,
    pub longitude: &'a f64,
    pub postal_code: &'a str,
    pub continent_code: &'a str,
    pub continent_name: &'a str,
    pub country_code: &'a str,
    pub country_name: &'a str,
    pub region_code: &'a str,
    pub region_name: &'a str,
    pub province_code: &'a str,
    pub province_name: &'a str,
    pub city_name: &'a str,
    pub timezone: &'a str,
}

#[derive(Deserialize, Debug)]
struct QueryParams {
    ip: Option<String>,
    lang: Option<String>,
    callback: Option<String>,
}

fn ip_address_to_resolve(
    ip: Option<String>,
    headers: &HeaderMap,
    remote_addr: Option<&str>,
) -> String {
    ip.filter(|ip_address| {
        ip_address.parse::<Ipv4Addr>().is_ok() || ip_address.parse::<Ipv6Addr>().is_ok()
    })
    .or_else(|| {
        headers
            .get("X-Real-IP")
            .map(|s| s.to_str().unwrap().to_string())
    })
    .or_else(|| {
        remote_addr
            .map(|ip_port| ip_port.split(':').take(1).last().unwrap())
            .map(|ip| ip.to_string())
    })
    .expect("unable to find ip address to resolve")
}

fn get_language(lang: Option<String>) -> String {
    lang.unwrap_or_else(|| String::from("en"))
}

struct Db {
    // db: Arc<Reader<Mmap>>,
    db: Arc<Reader<Vec<u8>>>,
}

async fn index(
    req: HttpRequest,
    data: web::Data<Db>,
    web::Query(query): web::Query<QueryParams>
) -> HttpResponse {
    let language = get_language(query.lang);
    let ip_address = ip_address_to_resolve(
        query.ip,
        req.headers(),
        req.connection_info().realip_remote_addr()
    );

    let lookup: Result<City, MaxMindDBError> = data.db.lookup(ip_address.parse().unwrap());

    let geoip = match lookup {
        Ok(geoip) => {
            let region = geoip
                .subdivisions
                .as_ref()
                .filter(|subdivs| !subdivs.is_empty())
                .and_then(|subdivs| subdivs.get(0));

            let province = geoip
                .subdivisions
                .as_ref()
                .filter(|subdivs| subdivs.len() > 1)
                .and_then(|subdivs| subdivs.get(1));

            let res = ResolvedIPResponse {
                ip_address: &ip_address,
                latitude: geoip
                    .location
                    .as_ref()
                    .and_then(|loc| loc.latitude.as_ref())
                    .unwrap_or(&0.0),
                longitude: geoip
                    .location
                    .as_ref()
                    .and_then(|loc| loc.longitude.as_ref())
                    .unwrap_or(&0.0),
                postal_code: geoip
                    .postal
                    .as_ref()
                    .and_then(|postal| postal.code.as_ref())
                    .unwrap_or(&""),
                continent_code: geoip
                    .continent
                    .as_ref()
                    .and_then(|cont| cont.code.as_ref())
                    .unwrap_or(&""),
                continent_name: geoip
                    .continent
                    .as_ref()
                    .and_then(|cont| cont.names.as_ref())
                    .and_then(|names| names.get::<str>(language.borrow()))
                    .unwrap_or(&""),
                country_code: geoip
                    .country
                    .as_ref()
                    .and_then(|country| country.iso_code.as_ref())
                    .unwrap_or(&""),
                country_name: geoip
                    .country
                    .as_ref()
                    .and_then(|country| country.names.as_ref())
                    .and_then(|names| names.get::<str>(language.borrow()))
                    .unwrap_or(&""),
                region_code: region
                    .and_then(|subdiv| subdiv.iso_code.as_ref())
                    .unwrap_or(&""),
                region_name: region
                    .and_then(|subdiv| subdiv.names.as_ref())
                    .and_then(|names| names.get::<str>(language.borrow()))
                    .unwrap_or(&""),
                province_code: province
                    .and_then(|subdiv| subdiv.iso_code.as_ref())
                    .unwrap_or(&""),
                province_name: province
                    .and_then(|subdiv| subdiv.names.as_ref())
                    .and_then(|names| names.get::<str>(language.borrow()))
                    .unwrap_or(&""),
                city_name: geoip
                    .city
                    .as_ref()
                    .and_then(|city| city.names.as_ref())
                    .and_then(|names| names.get::<str>(language.borrow()))
                    .unwrap_or(&""),
                timezone: geoip
                    .location
                    .as_ref()
                    .and_then(|loc| loc.time_zone.as_ref())
                    .unwrap_or(&""),
            };
            serde_json::to_string(&res)
        }
        Err(_) => serde_json::to_string(&NonResolvedIPResponse {
            ip_address: &ip_address,
        }),
    }
    .unwrap();

    match query.callback {
        Some(callback) => HttpResponse::Ok()
            .content_type("application/javascript; charset=utf-8")
            .body(format!(";{}({});", callback, geoip)),
        None => HttpResponse::Ok()
            .content_type("application/json; charset=utf-8")
            .body(geoip),
    }
}

#[actix_web::main]
async fn main() {
    dotenv::from_path(".env").ok();

    let host = env::var("GEOIP_RS_HOST").unwrap_or_else(|_| String::from("0.0.0.0"));
    let port = env::var("NOMAD_PORT_http").unwrap_or_else(|_| String::from("8080"));

    println!("Listening on http://{}:{}", host, port);

    let db = Arc::new(Reader::open_readfile(
        env::var("GEOIP_RS_DB_PATH").unwrap_or(String::from("GeoLite2-City.mmdb"))
    ).unwrap());

    HttpServer::new(move || {
        App::new()
            .data(Db { db: db.clone() })
            .wrap(Cors::default().send_wildcard())
            .route("/", web::route().to(index))
    })
    .bind(format!("{}:{}", host, port))
    .unwrap_or_else(|_| panic!("Can not bind to {}:{}", host, port))
    .run()
    .await
    .unwrap();
}
