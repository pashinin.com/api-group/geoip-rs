# Build Stage
# docker build -t registry.gitlab.com/pashinin.com/api-group/geoip-rs:master .
FROM pashinin/rust-runtime:2021-01-26
COPY target/release/geoip-rs ./
ENTRYPOINT ["/geoip-rs"]
CMD ["./geoip-rs"]
